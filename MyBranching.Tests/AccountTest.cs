using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyBranching;
using System;

namespace MyBranching.Tests
{
    [TestClass]
    public class AccountTest
    {
        Action action = () => Console.WriteLine("Unfronzen");

        [TestMethod]
        public void Account_Deposit_Successful_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.Deposit(234.53M);

            Assert.AreEqual(244.53M, acct.Balance);
        }

        [TestMethod]
        public void Account_Withdrawal_Verified_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.HolderVerified();
            acct.Withdraw(5.25M);

            Assert.AreEqual(4.75M, acct.Balance);
        }

        [TestMethod]
        public void Account_Withdrawal_Unverified_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.Withdraw(5.25M);

            Assert.AreEqual(10M, acct.Balance);
        }

        [TestMethod]
        public void Account_CloseAcct_Unverified_Test()
        {
            Account acct = new Account(action);
            acct.Closed();
            acct.Deposit(5.25M);

            Assert.AreEqual(5.25M, acct.Balance);
        }

        [TestMethod]
        public void Account_Withdrawal_Closed_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.HolderVerified();
            acct.Closed();
            acct.Withdraw(5.25M);

            Assert.AreEqual(10M, acct.Balance);
        }

        [TestMethod]
        public void Account_Deposit_Closed_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.HolderVerified();
            acct.Closed();
            acct.Deposit(5.25M);

            Assert.AreEqual(10M, acct.Balance);
        }

        [TestMethod]
        public void Account_Deposit_Unfreeze_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.HolderVerified();
            acct.Freeze();
            acct.Deposit(5.25M);

            Assert.AreEqual(15.25M, acct.Balance);
        }

        [TestMethod]
        public void Account_Withdraw_Unfreeze_Test()
        {
            Account acct = new Account(action);
            acct.Deposit(10M);
            acct.HolderVerified();
            acct.Freeze();
            acct.Withdraw(5.25M);

            Assert.AreEqual(4.75M, acct.Balance);
        }
    }
}
