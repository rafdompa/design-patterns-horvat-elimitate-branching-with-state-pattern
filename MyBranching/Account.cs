﻿using MyBranching.States;
using System;

namespace MyBranching
{
    public class Account
    {

        public Decimal Balance { get; private set; }

        private IAccountState State { get; set; }

        public Account(Action onUnfreeze)
        {
            this.State = new NotVerified(onUnfreeze);
        }
        
        public void Deposit(Decimal amount)
        {
            this.State = this.State.Deposit(() => { this.Balance += amount; });
        }

        public void Withdraw(Decimal amount)
        {
            this.State = this.State.Withdraw( () => { this.Balance -= amount; });
        }

        public void HolderVerified()
        {
            this.State = this.State.HolderVerified();
        }

        public void Closed()
        {
            this.State = this.State.Closed();
        }

        public void Freeze()
        {
            this.State = this.State.Freeze();
        }
    }
}
