﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBranching.States
{
    class Active : IAccountState
    {
        private Action OnUnfreeze { get; }

        public Active(Action onUnfreeze)
        {
            this.OnUnfreeze = onUnfreeze;
        }

        public IAccountState Deposit(Action addToBalance)
        {
            addToBalance();
            return this;
        }

        public IAccountState Withdraw(Action substractBalance)
        {
            substractBalance();
            return this;
        }

        public IAccountState Freeze() => new Frozen(this.OnUnfreeze);

        public IAccountState HolderVerified() => this;

        public IAccountState Closed() => new Closed();
    }
}
