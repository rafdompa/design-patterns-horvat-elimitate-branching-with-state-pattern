﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBranching.States
{
    class Closed : IAccountState
    {
        public IAccountState Deposit(Action addToBalance) => this;

        public IAccountState Freeze() => this;

        public IAccountState HolderVerified() => this;

        public IAccountState Withdraw(Action substractBalance) => this;

        IAccountState IAccountState.Closed() => this;
    }
}
