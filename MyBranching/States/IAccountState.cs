﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBranching.States
{
    interface IAccountState
    {
        IAccountState Deposit(Action addToBalance);
        IAccountState Withdraw(Action substractBalance);
        IAccountState Freeze();
        IAccountState HolderVerified();
        IAccountState Closed();
    }
}
