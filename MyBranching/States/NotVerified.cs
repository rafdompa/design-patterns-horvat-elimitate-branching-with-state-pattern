﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBranching.States
{
    class NotVerified : IAccountState
    {
        private Action onUnfreeze;
        public NotVerified(Action onUnfreeze)
        {
            this.onUnfreeze = onUnfreeze;
        }
        public IAccountState Closed() => this;

        public IAccountState Deposit(Action substractBalance)
        {
            substractBalance();
            return this;
        }

        public IAccountState Freeze() => this;

        public IAccountState HolderVerified() => new Active(this.onUnfreeze);

        public IAccountState Withdraw(Action withdrawBalance) => this;
    }
}
